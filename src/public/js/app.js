let i = 0;
const typeText = "Welcome to Jakeob's Website";
const speed = 150;

function typeWriter() {
  if (i < typeText.length) {
    document.getElementById("typewriter").innerHTML += (typeText.charAt(i));
    i++;
    setTimeout(typeWriter, speed);
  }
}
typeWriter();


