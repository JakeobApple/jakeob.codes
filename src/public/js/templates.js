class Header extends HTMLElement {
  constructor() {
    super();
  }
}
const headerCreate = document.createElement('template');
headerCreate.innerHTML = '  <header>\n' +
  '    <nav class="top-navigation">\n' +
  '      <div class="top-navigation-contents">\n' +
  '        <ul class="title-contents">\n' +
  '          <li><a href="https://jakeob.codes/">jakeob.codes</a></li>\n' +
  '        </ul>\n' +
  '        <ul>\n' +
  '          <li><a href="https://jakeob.codes/projects/">Projects</a></li>\n' +
  '          <li><a href="https://blog.jakeob.codes/">Blog</a></li>\n' +
  '          <li><a href="https://jakeob.codes/about/">About</a></li>\n' +
  '        </ul>\n' +
  '      </div>\n' +
  '    </nav>\n' +
  '  </header>';

document.body.prepend(headerCreate.content);

const footerCreate = document.createElement('template');
footerCreate.innerHTML =
  '<footer>\n' +
  ' <nav class="footer-navigation">' +
  '   <div class="footer-block widgets-wrapper">' +
  '     <div class="footer-column">' +
  '       <h2>Contact</h2>' +
  '        <ul>' +
  '          <li><a href="mailto:jacob@jakeob.codes">jacob@jakeob.codes</a></li>' +
  '          <li><a></a></li>' +
  '        </ul>' +
  '     </div>' +
  '     <div class="footer-column">' +
  '       <h2>Social</h2>' +
  '       <ul>' +
  '         <li><a><i class="fa-brands fa-discord"></i>  @JakeobApple</a></li>' +
  '         <li><a href="https://gitlab.com/jakeobapple"><i class="fa-brands fa-gitlab"></i>  @JakeobApple</a></li>' +
  '         <li><a href="https://youtube.com/@jakeobapple"><i class="fa-brands fa-youtube"></i>  @JakeobApple</a></li>' +
  '       </ul>' +
  '     </div>' +  '     ' +
  '     <div class="footer-column">' +
  '       <h2>Info</h2>' +
  '       <ul>' +
  '         <li><a href="https://jakeob.codes/update">Changelogs</a></li>' +
  '         <li><a href="https://jakeob.codes/sitemap">Sitemap</a></li>' +
  '         <li><a href="https://jakeob.codes/about">About</a></li>' +
  '       </ul>' +
  '     </div>' +
  '     <div class="footer-legal footer-column">' +
  '       <h2></h2>' +
  '       <ul>' +
  '         <li><a>Privacy Notice</a></li>' +
  '         <li><a>Copyright © Jacob Gonzalez 2022 - 2024</a></li>' +
  '       </ul>' +
  '     </div>' +
  '   </div>' +
  ' </nav>' +
  '</footer>';

document.body.appendChild(footerCreate.content);
